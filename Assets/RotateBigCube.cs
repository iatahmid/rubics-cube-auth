using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBigCube : MonoBehaviour
{
    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;

    private Vector3 previousMousePosition;

    public GameObject target;

    private float speed = 200f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Swipe();
        Drag();
    }

    private void Swipe()
    {
        if (Input.GetMouseButtonDown(1))
        {
            // get the 2D position of the mouse when first pressed
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            print(firstPressPos);
        }

        if (Input.GetMouseButtonUp(1))
        {
            // get the 2D position of the second mouse press up
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            // get the current swipe by subtracting the current position to the previous position
            currentSwipe = secondPressPos - firstPressPos;

            // normalize the swipe
            currentSwipe.Normalize();

            if (RightSwipe(currentSwipe))
            {
                target.transform.Rotate(0, -90, 0, Space.World);
            }
            else if (LeftSwipe(currentSwipe))
            {
                target.transform.Rotate(0, 90, 0, Space.World);
            }
            else if (UpLeftSwipe(currentSwipe))
            {
                target.transform.Rotate(90, 0, 0, Space.World);
            }
            else if (UpRightSwipe(currentSwipe))
            {
                target.transform.Rotate(0, 0, -90, Space.World);
            }
            else if (DownLeftSwipe(currentSwipe))
            {
                target.transform.Rotate(0, 0, 90, Space.World);
            }
            else if (DownRightSwipe(currentSwipe))
            {
                target.transform.Rotate(-90, 0, 0, Space.World);
            }
        }
    }

    private void Drag()
    {
        if (Input.GetMouseButton(1))
        {
            var mouseDelta = Input.mousePosition - previousMousePosition;
            mouseDelta *= 0.2f;

            transform.rotation = Quaternion.Euler(mouseDelta.y, -mouseDelta.x, 0) * transform.rotation;
        }
        else
        {
            if (transform.rotation != target.transform.rotation)
            {
                var step = speed * Time.deltaTime;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, target.transform.rotation, step);
            }
        }

        previousMousePosition = Input.mousePosition;
    }

    private bool RightSwipe(Vector2 swipe)
    {
        return currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f;
    }

    private bool LeftSwipe(Vector2 swipe)
    {
        return currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f;
    }

    private bool UpLeftSwipe(Vector2 swipe)
    {
        return currentSwipe.x < 0 && currentSwipe.y > 0;
    }

    private bool UpRightSwipe(Vector2 swipe)
    {
        return currentSwipe.x > 0 && currentSwipe.y > 0;
    }

    private bool DownLeftSwipe(Vector2 swipe)
    {
        return currentSwipe.x < 0 && currentSwipe.y < 0;
    }

    private bool DownRightSwipe(Vector2 swipe)
    {
        return currentSwipe.x > 0 && currentSwipe.y < 0;
    }
}
